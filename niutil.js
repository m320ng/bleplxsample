import crc from 'crc';

export function crc16(pt) {
  const checksum = crc.crc16(pt);
  return checksum;
}

export function crc16Hex(hexString) {
  const pt = new Uint8Array(
    hexString
      .replace(/ /g, '')
      .match(/[\da-f]{2}/gi)
      .map(function (h) {
        return parseInt(h, 16);
      }),
  );
  console.log('pt', pt);
  const checksum = crc.crc16(pt);
  return shortToByteArray(checksum);
}

export function intToByteArray(/*int*/ int) {
  // we want to represent the input as a 8-bytes array
  let byteArray = [0, 0, 0, 0];

  for (let index = 0; index < byteArray.length; index++) {
    const byte = int & 0xff;
    byteArray[index] = byte;
    int = (int - byte) / 256;
  }
  return byteArray;
}

export function shortToByteArray(/*short*/ short) {
  // we want to represent the input as a 8-bytes array
  let byteArray = [0, 0];

  for (let index = 0; index < byteArray.length; index++) {
    const byte = short & 0xff;
    byteArray[index] = byte;
    short = (short - byte) / 256;
  }
  return byteArray;
}

export function toHexString(byteArray) {
  return Array.from(byteArray, function (byte) {
    return ('0' + (byte & 0xff).toString(16)).slice(-2);
  }).join('');
}
