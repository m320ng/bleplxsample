import React, { useEffect, useState } from 'react';
import { PermissionsAndroid } from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import { Buffer } from 'buffer';
import SimpleCrypt from 'react-native-simple-crypto';
import { crc16Hex, toHexString } from './niutil';
import { VStack, Center, FlatList, Box, Text, HStack, Button, Spacer, Pressable, Spinner, Heading } from 'native-base';

const svcUUID = '62110e45-7399-452b-ac7c-44b6111d6150';
const txUUID = '62110e46-7399-452b-ac7c-44b6111d6150';
const rxUUID = '62110e47-7399-452b-ac7c-44b6111d6150';

const base64ToHex = (str) => {
  const decoded = Buffer.from(str, 'base64').toString('hex');
  return decoded;
};

const makePassword = async (hex) => {
  const typedArray = new Uint8Array(
    hex.match(/[\da-f]{2}/gi).map(function (h) {
      return parseInt(h, 16);
    }),
  );
  const hashedBuffer = await SimpleCrypt.SHA.sha256(typedArray.buffer);
  const code = [...new Uint8Array(hashedBuffer)].map((x) => x.toString(16).padStart(2, '0')).join('');
  //console.log('code',code);
  const num = parseInt(code.substring(0, 8), 16);
  //console.log('num',num);
  const pass = num % 999999;
  //console.log('pass',pass);
  return pass;
};

const manager = new BleManager();

const Home = () => {
  const [selectedDevice, setSelectedDevice] = useState(null);
  const [scanning, setScanning] = useState(false);
  const [connecting, setConnecting] = useState(false);
  const [scanningDevices, setScanningDevices] = useState([]);
  const [scanningDevicesWithPassCode, setScanningDevicesWithPassCode] = useState([]);
  const [passcode, setPasscode] = useState();
  const [writeHex, setWriteHex] = useState();
  const [errorMessage, setErrorMessage] = useState(null);
  const [writeErrorMessage, setWriteErrorMessage] = useState(null);
  const [readErrorMessage, setReadErrorMessage] = useState(null);
  const [readData, setReadData] = useState();
  const [permissionStatus, setPermissionStatus] = useState();

  const requestPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.BLUETOOTH);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        setPermissionStatus('granted');
      } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
        setPermissionStatus('denied');
      } else {
        //console.log('never-ask-again');
      }
      {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.BLUETOOTH_SCAN);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          setPermissionStatus('granted');
        } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
          setPermissionStatus('denied');
        } else {
          //console.log('never-ask-again');
        }
      }
      {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          setPermissionStatus('granted');
        } else if (granted === PermissionsAndroid.RESULTS.DENIED) {
          setPermissionStatus('denied');
        } else {
          //console.log('never-ask-again');
        }
      }
    } catch (err) {
      console.error(err);
    }
  };

  const disconnect = async (deviceUUID) => {
    const isDeviceConnected = await manager.isDeviceConnected(deviceUUID);
    console.log('isDeviceConnected', isDeviceConnected);
    if (isDeviceConnected) {
      await manager.cancelDeviceConnection(deviceUUID);
    }
    setSelectedDevice(null);
  };

  const clearError = () => {
    setWriteHex('');
    setErrorMessage('');
    setWriteErrorMessage('');
    setReadErrorMessage('');
  };

  const monitorData = async () => {
    const subscription = await manager.monitorCharacteristicForDevice(
      selectedDevice,
      svcUUID,
      rxUUID,
      (error, characteristic) => {
        if (error) {
          console.log('readData error', error);
          setReadErrorMessage(error.reason);
          return;
        }
        console.log('characteristic.value', characteristic.value);
        const valueInBase64 = characteristic.value;
        setReadData(characteristic.value);
        const valueInString = Buffer.from(valueInBase64, 'base64').toString('hex');
        console.log('valueInString', valueInString);
      },
    );
    console.log('subscription', subscription);
  };
  const writeData1 = async () => {
    let hexString = '51 F8';
    hexString += '00 00 03';
    hexString += '00';
    hexString += '00 0B';
    hexString += '01 00 01 1006 00 04 00 0A 00 00';
    await writeData(hexString);
  };
  const writeData2 = async () => {
    let hexString = '51 F8';
    hexString += '00 00 03';
    hexString += '00';
    hexString += '00 07';
    hexString += '01 00 01 F0 06 00 00';
    await writeData(hexString);
  };

  const writeData = async (hexString) => {
    try {
      const checksumBuff = crc16Hex(hexString);
      console.log('checksumBuff', toHexString(checksumBuff));
      hexString += toHexString(checksumBuff);

      console.log('hexString', hexString);
      hexString = hexString.replace(/ /g, '');
      setWriteHex(hexString);
      const data = Buffer.from(hexString, 'hex').toString('base64');
      console.log('base64data', data);

      const result = await manager.writeCharacteristicWithResponseForDevice(selectedDevice, svcUUID, txUUID, data);
      console.log('result', result);
    } catch (e) {
      console.log(e);
      setWriteErrorMessage(e.message);
    }
  };

  const connectDirectDevice = async (deviceUUID, passcode) => {
    clearError();
    let device = null;
    try {
      manager.stopDeviceScan();
      setScanning(false);

      setPasscode(('000' + passcode).substring(('000' + passcode).length - 6));
      setConnecting(true);
      console.log('connectToDevice..', deviceUUID);
      device = await manager.connectToDevice(deviceUUID);
      setSelectedDevice(device.id);
      manager.onDeviceDisconnected(device.id, () => {
        setSelectedDevice(null);
      });

      console.log('Discovering services and characteristics');
      await device.discoverAllServicesAndCharacteristics();
      const services = await manager.servicesForDevice(device.id);
      for (const service of services) {
        const characteristics = await service.characteristics();
        characteristics.forEach((characteristic) => {
          if (characteristic.uuid == txUUID || characteristic.uuid == rxUUID) {
            console.log('characteristic', characteristic.uuid);
            console.log(' W ', characteristic.isWritableWithResponse);
            console.log(' W ', characteristic.isWritableWithoutResponse);
            console.log(' R ', characteristic.isReadable);
            console.log(' I ', characteristic.isIndicatable);
            console.log(' N ', characteristic.isNotifiable);
          }
        });
      }
      setConnecting(false);
    } catch (e) {
      console.log('e', e);
      setErrorMessage(e.message);
      setConnecting(false);
      setSelectedDevice(null);
      if (device) {
        const isDeviceConnected = await manager.isDeviceConnected(device.id);
        console.log('isDeviceConnected', isDeviceConnected);
        if (isDeviceConnected) {
          await device.cancelConnection();
        }
      }
    }
  };

  const scanAndConnect = async () => {
    clearError();
    //await disconnect(selectedDevice);
    setScanning(true);
    setScanningDevices([]);

    manager.startDeviceScan([], null, (error, device) => {
      if (error) {
        console.log('Error occurred while scanning', error);
        setScanning(false);
        return;
      }
      if (!device.name) return;

      setScanningDevices((prev) => {
        const newdevice = {
          id: device.id,
          name: device.name,
          localName: device.localName,
          rssi: device.rssi,
          manufacturerData: device.manufacturerData ? base64ToHex(device.manufacturerData) : null,
        };
        const list = [newdevice, ...prev.filter((x) => x.id != device.id)];
        list.sort((a, b) => a.name.localeCompare(b.name));
        return list;
      });
      //connectDevice(device);
    });
  };

  const setupScanningDevicesWithPassCode = async () => {
    for (const device of scanningDevices.filter((x) => !!x.manufacturerData)) {
      device.passcode = await makePassword(device.manufacturerData);
    }
    setScanningDevicesWithPassCode([...scanningDevices]);
  };

  useEffect(() => {
    setupScanningDevicesWithPassCode();
  }, [scanningDevices]);

  useEffect(() => {
    if (selectedDevice) {
      disconnect(selectedDevice);
    }
    requestPermission();
    return () => {
      manager.stopDeviceScan();
      if (selectedDevice) {
        disconnect(selectedDevice);
      }
    };
  }, []);

  console.log('scanningDevices', scanningDevices);

  return (
    <VStack>
      <Center space={4} alignItems="center">
        {!!selectedDevice ? (
          <Button
            onPress={() => {
              disconnect(selectedDevice);
            }}
            disabled={scanning}
          >
            디바이스 연결종료
          </Button>
        ) : (
          <HStack>
            {!scanning && !connecting && (
              <Button
                onPress={() => {
                  scanAndConnect();
                }}
                disabled={scanning}
              >
                누리 디바이스 검색
              </Button>
            )}
            {scanning && (
              <Button
                onPress={() => {
                  manager.stopDeviceScan();
                  setScanning(false);
                }}
                disabled={!scanning}
              >
                누리 디바이스 검색중지
              </Button>
            )}
          </HStack>
        )}
        {scanning && (
          <>
            <Spinner accessibilityLabel="Loading posts" />
            <Heading color="primary.500" fontSize="md">
              디바이스 검색중
            </Heading>
          </>
        )}
        {connecting && (
          <>
            <Spinner color="green.500" accessibilityLabel="Loading posts" />
            <Heading color="green.500" fontSize="md">
              디바이스 연결중
            </Heading>
          </>
        )}

        {!!selectedDevice && (
          <>
            <Heading>핀코드 {passcode}</Heading>
            <Text>{selectedDevice} 연결됨</Text>
            <VStack space="1">
              <Button
                onPress={() => {
                  monitorData();
                }}
              >
                {rxUUID} 구독
              </Button>
              <Button
                onPress={() => {
                  writeData2();
                }}
              >
                {txUUID} WRITE (get light on)
              </Button>
              <Button
                onPress={() => {
                  writeData1();
                }}
              >
                {txUUID} WRITE (get modem info(샘플))
              </Button>
            </VStack>
          </>
        )}
        {writeHex ? (
          <>
            <Text>[write data] </Text>
            <Text>{writeHex}</Text>
          </>
        ) : (
          <></>
        )}
        {readData ? (
          <>
            <Text>[read data] </Text>
            <Text>{readData}</Text>
          </>
        ) : (
          <></>
        )}
        {errorMessage ? <Text>{errorMessage}</Text> : <></>}
        {readErrorMessage ? <Text>{readErrorMessage}</Text> : <></>}
        {writeErrorMessage ? <Text>{writeErrorMessage}</Text> : <></>}
      </Center>
      <FlatList
        data={scanningDevicesWithPassCode}
        renderItem={({ item }) => (
          <Pressable
            onPress={() => {
              if (connecting) return;
              connectDirectDevice(item.id, item.passcode);
            }}
            disabled={!item.passcode || connecting}
          >
            {({ pressed }) => (
              <Box
                borderBottomWidth="1"
                borderColor="coolGray.200"
                pl="4"
                pr="5"
                py="2"
                opacity={item.passcode || pressed ? 1 : 0.5}
              >
                <HStack space={3} justifyContent="space-between">
                  <Box size="48px">{item.rssi}</Box>
                  <VStack flex={1}>
                    <Text>{item.name}</Text>
                    <Text numberOfLines={1}>{item.id}</Text>
                  </VStack>
                  {item.passcode && (
                    <VStack size={'80px'}>
                      <Text>passcode</Text>
                      <Text>{item.passcode}</Text>
                    </VStack>
                  )}
                </HStack>
              </Box>
            )}
          </Pressable>
        )}
      />
    </VStack>
  );
};

export default Home;
