import React from 'react';
import { NativeBaseProvider, Box } from 'native-base';
import Home from './Home';

const App = () => {
  return (
    <NativeBaseProvider>
      <Box safeArea>
        <Home />
      </Box>
    </NativeBaseProvider>
  );
};

export default App;
